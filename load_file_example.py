from PyQt5 import QtWidgets, QtWebEngineWidgets, QtCore
import sys, os

class WebPage(QtWebEngineWidgets.QWebEnginePage):
    def __init__(self,parent=None):
        self.parent = parent
        QtWebEngineWidgets.QWebEnginePage.__init__(self,parent)

    def acceptNavigationRequest(self,url,*args):
        # Override link
        
        if url.toString().startswith('http'):
            return 1
        else:
            self.parent.urlClicked(url)
            return 0

    def javaScriptConsoleMessage(self, code, msg, line, *args):
        '''QWebEnginePage that prints Javascript errors to stderr.'''
        typ = ''
        if code != 0: typ=' ERROR'
        print('js>>%s %s  (line %s)' % (typ, msg, line))

class App(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, parent=None):
        QtWebEngineWidgets.QWebEngineView.__init__(self, parent)
        self.web_page = WebPage(self)
        self.setPage(self.web_page)

        self.web_page.setHtml('hi <a href="https://maps.google.com">world</a>')
        
        with open('home.html') as f:
            self.setHtml(f.read(),QtCore.QUrl('file:///'+os.path.abspath('home.html')))
        
    def urlClicked(self,url):
        '''Override the Url'''
##        print(url)
        if url.toString().startswith('action'):
            act = url.toString().split('action:')[1]
            if act == 'alert':
                QtWidgets.QMessageBox.information(self,'Info','Python Alert')


def runui():
    '''Launch the app'''
    sys.excepthook = errorHandler
    qapp = QtWidgets.QApplication(sys.argv)
    app = App()
    app.show()
    sys.exit(qapp.exec_())

def errorHandler(*exc_info):
    '''Handle errors so Qt app doesn't crash on error'''
    import traceback
    print("".join(traceback.format_exception(*exc_info)), file=sys.stderr)

if __name__ == "__main__":
    runui()
