from PyQt5 import QtWidgets, QtWebEngineWidgets, QtCore
import sys, os

class App(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, parent=None):
        QtWebEngineWidgets.QWebEngineView.__init__(self, parent)
        self.load(QtCore.QUrl('http://doc.qt.io/qt-5/'))

def runui():
    '''Launch the app'''
    sys.excepthook = errorHandler
    qapp = QtWidgets.QApplication(sys.argv)
    app = App()
    app.show()
    sys.exit(qapp.exec_())

def errorHandler(*exc_info):
    '''Handle errors so Qt app doesn't crash on error'''
    import traceback
    print("".join(traceback.format_exception(*exc_info)), file=sys.stderr)

if __name__ == "__main__":
    runui()
