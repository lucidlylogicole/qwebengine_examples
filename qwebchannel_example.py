from PyQt5 import QtWidgets, QtWebEngineWidgets, QtCore
from PyQt5.QtWebChannel import QWebChannel
import sys, os

# QWebchannel Example 
#   - utilized answer from https://stackoverflow.com/questions/39544089/how-can-i-access-python-code-from-javascript-in-pyqt-5-7

class WebPage(QtWebEngineWidgets.QWebEnginePage):
    def __init__(self,parent=None):
        self.parent = parent
        QtWebEngineWidgets.QWebEnginePage.__init__(self,parent)
        
    def javaScriptConsoleMessage(self, code, msg, line, *args):
        '''QWebEnginePage that prints Javascript errors to stderr.'''
        typ = ''
        if code != 0: typ=' ERROR'
        print('js>>%s %s  (line %s)' % (typ, msg, line))


class App(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, parent=None):
        QtWebEngineWidgets.QWebEngineView.__init__(self, parent)
        self.web_page = WebPage(self)
        self.setPage(self.web_page)

        self.web_page.setHtml('''
            <script src="qrc:///qtwebchannel/qwebchannel.js"></script>
            hi <a href="#" onclick="hiworld()">world</a>
        
            <script>
                // Setup WebChannel
                new QWebChannel(qt.webChannelTransport, function (channel) {
                    window.handler = channel.objects.handler;
                    
                    // Run Initial Python (must be in this function)
                    window.handler.test('mark');
                    window.handler.testdict({a:2});
                    window.handler.testreturn('js input',function(returnval){console.log(returnval)})
                });
                
                function hiworld() {
                    window.handler.test('world');
                }
                
            </script>
        
        ''')

def errorHandler(*exc_info):
    import traceback
    print("".join(traceback.format_exception(*exc_info)), file=sys.stderr)


# QWebChannel Handler
class CallHandler(QtCore.QObject):
    @QtCore.pyqtSlot(str)
    def test(self,text=''):
        print('call received',text)

    @QtCore.pyqtSlot(dict)
    def testdict(self,d={}):
        '''Pass a dictionary object from javascript to Python'''
        print('dict received',d)

    @QtCore.pyqtSlot(str,result=str)
    def testreturn(self,input):
        '''Pass a dictionary object from javascript to Python'''
        print('received',input)
        return 'python out'

def runui():
    # Create App
    sys.excepthook = errorHandler
    qapp = QtWidgets.QApplication(sys.argv)
    app = App()
    
    # Setup QWebChannel on web page
    app.channel = QWebChannel()
    app.handler = CallHandler()
    app.channel.registerObject('handler', app.handler)
    app.page().setWebChannel(app.channel)
    
    # View App
    app.show()
    sys.exit(qapp.exec_())

if __name__ == "__main__":
    runui()
