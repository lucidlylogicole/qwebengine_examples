# QWebEngineView Examples

## Requirements
- Python (only tested with Python 3.6, but may work with 2.7+)
- PyQt5

## Examples
- [Load HTML File](load_file_example.py) - Load a local html file
- [Load URL](load_url_example.py) - Start the browser with a url
- [WebChannel Example](qwebchannel_example.py) - Call functions between Python and JavaScript using QWebChannel